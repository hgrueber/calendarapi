﻿using Calendar.API.Models;
using System;
using System.Collections.Generic;

namespace Calendar.API.Helpers
{
    /// <summary>
    /// Helper class with date and time handling.
    /// </summary>
    public static class DateHelper
    {
        /// <summary>
        /// Resolve CalendarEvent list with time filter
        /// </summary>
        /// <param name="eventsToResolve">List of CalendarEVents to be resolved</param>
        /// <param name="timeFilter">Time filter</param>
        /// <returns>Resolved list of calendar events</returns>
        public static List<CalendarEvent> ResolveValues(List<CalendarEvent> eventsToResolve, TimeSpan timeFilter)
        {
            var resolvedList = new List<CalendarEvent>();

            foreach (CalendarEvent resultEvent in eventsToResolve)
            {
                var endDate = resultEvent.EventDate;
                var comparerDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, timeFilter.Hours, timeFilter.Minutes, 0);

                endDate = endDate.AddMinutes(resultEvent.EventDuration);

                // if adding duration exceeds a day then add day to comparer
                if (endDate.Year > resultEvent.EventDate.Year || endDate.Month > resultEvent.EventDate.Month || endDate.Day > resultEvent.EventDate.Day) {
                    if (comparerDate.Hour <= resultEvent.EventDate.Hour && comparerDate.Minute <= resultEvent.EventDate.Minute)
                    {

                        comparerDate = comparerDate.AddDays(1);
                    }
                }

                if (comparerDate >= resultEvent.EventDate && comparerDate <= endDate)
                {
                    resolvedList.Add(resultEvent);
                }
            }

            return resolvedList;
        }
    }

}