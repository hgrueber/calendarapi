﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Calendar.API.Models
{
    public class CalendarEvent
    {
        [Key]
        public int ID { get; set; } // database identifier
        public string EventName { get; set; }
        public DateTime EventDate { get; set; }
        public int EventDuration { get; set; } // in minutes
    }
}
