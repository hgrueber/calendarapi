﻿using System;
using System.Collections.Generic;
using System.Text;
using Calendar.API.Models;

namespace Calendar.API.Data
{
    public interface ICalendarEventRepository
    {
        IEnumerable<CalendarEvent> GetEvents();
        IEnumerable<CalendarEvent> GetEvents(string eventName);
        IEnumerable<CalendarEvent> GetEvents(int? year, int? month, int? day, string time);
        CalendarEvent GetEvent(int id);
        void AddEvent(CalendarEvent calendarEvent);
        void EditEvent(CalendarEvent calendarEvent);
        void DeleteEvent(int id);

    }
}
