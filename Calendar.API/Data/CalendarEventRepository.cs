﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Calendar.API.Data;
using Calendar.API.Models;
using Microsoft.EntityFrameworkCore;
using Calendar.API.Helpers;

namespace Calendar.API.Data
{
    public class CalendarEventRepository : ICalendarEventRepository
    {
        private CalendarContext _context;

        /// <summary>
        /// Create new calendar repository.
        /// </summary>
        /// <param name="context">Import CalendarContext</param>
        public CalendarEventRepository(CalendarContext context)
        {
            this._context = context;
        }

        /// <summary>
        /// Get all events from database.
        /// </summary>
        /// <returns>List of events</returns>
        public IEnumerable<CalendarEvent> GetEvents()
        {
            return _context.CalendarEvents.ToList();
        }

        /// <summary>
        /// Get all events from database with date specific search parameters. Executes in year-month-day-time sequence.
        /// </summary>
        /// <param name="year">Event year</param>
        /// <param name="month">Event month</param>
        /// <param name="day">Event day</param>
        /// <param name="time">Event time, format HH:mm</param>
        /// <returns>List of events</returns>
        public IEnumerable<CalendarEvent> GetEvents(int? year, int? month, int? day, string time)
        {
            var timeSplitted = !String.IsNullOrEmpty(time) ? time.Split(':') : null;
            List<CalendarEvent> list = null;

            if (year.HasValue)
            {
                list = _context.CalendarEvents.Where(x => x.EventDate.Year == year.Value).ToList<CalendarEvent>();
            }

            if (month.HasValue)
            {
                if (list != null) // it is possible that list has already been created, search from it instead of database
                {
                    list = list.Where(x => x.EventDate.Month == month.Value).ToList<CalendarEvent>();
                }
                else
                {
                    list = _context.CalendarEvents.Where(x => x.EventDate.Month == month.Value).ToList<CalendarEvent>();
                }
            }

            if (day.HasValue)
            {
                if (list != null)
                {
                    list = list.Where(x => x.EventDate.Day == day.Value).ToList<CalendarEvent>();
                }
                else
                {
                    list = _context.CalendarEvents.Where(x => x.EventDate.Day == day.Value).ToList<CalendarEvent>();
                }
            }

            if (!string.IsNullOrEmpty(time))
            {

                try
                {
                    // Convert string to TimeSpan object and verify the formatting of time.
                    var tsTime = TimeSpan.ParseExact(time, "h\\:mm", null);

                    // find event between date and date + duration
                    if (list != null)
                    {
                        list = DateHelper.ResolveValues(list, tsTime);
                    }
                    else
                    {
                        var result = _context.CalendarEvents;

                        list = DateHelper.ResolveValues(result.ToList<CalendarEvent>(), tsTime);
                    }
                }
                catch(FormatException)
                {
                    throw new FormatException("Time format invalid, required format hh:mm");
                }
            }

            return list;
        }

        /// <summary>
        /// Get all events from database with specific search string.
        /// </summary>
        /// <param name="eventName">Name of event, not case sensitive</param>
        /// <returns>List of events</returns>
        public IEnumerable<CalendarEvent> GetEvents(string eventName)
        {
            return _context.CalendarEvents.Where(x => x.EventName.IndexOf(eventName, 0, StringComparison.OrdinalIgnoreCase) >= 0).ToList<CalendarEvent>();
        }

        /// <summary>
        /// Get specific event from database with database row identifier.
        /// </summary>
        /// <param name="id">Identifier value in database</param>
        /// <returns></returns>
        public CalendarEvent GetEvent(int id)
        {
            CalendarEvent eventResult = null;
            try
            {
                eventResult = _context.CalendarEvents.First(x => x.ID == id);
            }
            catch (InvalidOperationException e)
            {
                if (eventResult == null) { throw new InvalidOperationException("No event with such ID"); }
                else { throw e; }
            }

            return eventResult;
        }

        /// <summary>
        /// Add new avent to database.
        /// </summary>
        /// <param name="calendarEvent">New calendar event</param>
        public void AddEvent(CalendarEvent calendarEvent)
        {
            _context.CalendarEvents.Add(calendarEvent);
            _context.SaveChanges();
        }

        /// <summary>
        /// Edit existing event in database. Uses ID field as selecting variable.
        /// </summary>
        /// <param name="calendarEvent">Calendar event with edited values</param>
        public void EditEvent(CalendarEvent calendarEvent)
        {
            try
            {
                // Fetch the event with ID, throws an InvalidOperationException if not found
                var editEvent = _context.CalendarEvents.First(x => x.ID == calendarEvent.ID);

                // We need to use the queried event because it has an active (locked) instance ongoing and row cannot be used simultaneously
                editEvent.EventName = calendarEvent.EventName;
                editEvent.EventDate = calendarEvent.EventDate;
                editEvent.EventDuration = calendarEvent.EventDuration;

                _context.CalendarEvents.Update(editEvent);
                _context.SaveChanges();
            }
            catch(InvalidOperationException)
            {
                throw new InvalidOperationException("No event with such ID");
            }
        }

        /// <summary>
        /// Delete specific event from database with database row identifier.
        /// </summary>
        /// <param name="id">Identifier value in database</param>
        public void DeleteEvent(int id)
        {
            try
            {
                // Fetch the event with ID, throws an InvalidOperationException if not found
                var removeEvent = _context.CalendarEvents.First(x => x.ID == id);

                _context.CalendarEvents.Remove(removeEvent);
                _context.SaveChanges();
            }
            catch(InvalidOperationException)
            {
                throw new InvalidOperationException("No event with such ID");
            }
        }
    }
}
