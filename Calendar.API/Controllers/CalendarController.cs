﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Calendar.API.Data;
using Calendar.API.Models;
using Newtonsoft.Json;

namespace Calendar.API.Controllers
{
    [Route("api/[controller]")]
    public class CalendarController : ControllerBase
    {
        private const string UNEXPECTED_EXCEPTION = "Unexpected exception: ";

        private ICalendarEventRepository _repository;

        public CalendarController(ICalendarEventRepository repository)
        {
            this._repository = repository;
        }

        /// <summary>
        /// Main get action. Choose correct return path based on parameters.
        /// Name and id are single parameters. Date and time related can be used as a single parameter or combination of any.
        /// </summary>
        /// <param name="id">Get event by specific ID, can only be used as single parameter</param>
        /// <param name="name">Get event with name filter, can only be used as single parameter</param>
        /// <param name="year">Specify event year</param>
        /// <param name="month">Specify event month</param>
        /// <param name="day">Specify event day</param>
        /// <param name="time">Specify event time, format HH:mm</param>
        /// <returns>Action response and list of events</returns>
        [HttpGet]
        public IActionResult GetEvents(int? id, string name, int? year, int? month, int? day, string time)
        {
            IEnumerable<CalendarEvent> eventResults;

            try
            {
                if (!id.HasValue && !year.HasValue && !month.HasValue && !day.HasValue && String.IsNullOrEmpty(time) && String.IsNullOrEmpty(name))
                {
                    eventResults = _repository.GetEvents();
                }
                else if (!id.HasValue && !year.HasValue && !month.HasValue && !day.HasValue && String.IsNullOrEmpty(time) && !String.IsNullOrEmpty(name))
                {
                    eventResults = _repository.GetEvents(name);
                }
                else if(id.HasValue && !year.HasValue && !month.HasValue && !day.HasValue && String.IsNullOrEmpty(time) && String.IsNullOrEmpty(name))
                {
                    eventResults = new List<CalendarEvent>() { _repository.GetEvent(id.Value) };
                }
                else
                {
                    eventResults = _repository.GetEvents(year, month, day, time);
                }
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
            catch (FormatException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest($"{UNEXPECTED_EXCEPTION}{e.Message}");
            }

            return Ok(eventResults);
        }
        
        /// <summary>
        /// Insert new event action.
        /// </summary>
        /// <param name="calendarEvent">Calendar event from body</param>
        /// <returns>Action response</returns>
        [HttpPut]
        public IActionResult AddEvent([FromBody] CalendarEvent calendarEvent)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {
                _repository.AddEvent(calendarEvent);
            }
            catch(ArgumentNullException e)
            {
                return BadRequest(e.Message);
            }
            catch(Exception e)
            {
                return BadRequest($"{UNEXPECTED_EXCEPTION}{e.Message}");
            }

            return Ok();
        }

        /// <summary>
        /// Edit existing event action.
        /// </summary>
        /// <param name="calendarEvent">Calendar event from body</param>
        /// <returns>Action response</returns>
        [HttpPost]
        public IActionResult EditEvent([FromBody] CalendarEvent calendarEvent)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState);  }

            try
            {
                _repository.EditEvent(calendarEvent);
            }
            catch(InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest($"{UNEXPECTED_EXCEPTION}{e.Message}");
            }

            return Ok();
        }

        /// <summary>
        /// Delete existing event action.
        /// </summary>
        /// <param name="id">Identifier value of existing event</param>
        /// <returns>Action response</returns>
        [HttpDelete]
        public IActionResult DeleteEvent(int id)
        {
            try
            {
                _repository.DeleteEvent(id);
            }
            catch(InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
            catch(Exception e)
            {
                return BadRequest($"{UNEXPECTED_EXCEPTION}{e.Message}");
            }

            return Ok();
        }
    }
}
