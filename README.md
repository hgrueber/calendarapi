## Calendar API

This Calendar API uses ASP.NET Core 2.2 and Entity Framework Core 2.2.6

## Before building for the first time

Before building the project for the first time, do the following:

1. In Visual Studio open NuGet Package Manager Console from Tools > NuGet Package Manager > Package Manager Console
2. Run 'Add-Migration Calendar'
3. Run 'Update-Database'

This will create the database migrations and create a necessary table to the database.